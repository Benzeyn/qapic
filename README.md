# qapic

A basic http client that stores all configuration locally in a json file

## Path variables
Path variables can be defined with :
e.g. https://www.google.com/:pathVar
These can be edited in the request query pane

## Query parameters
Query parameters can be edited directly in the url or via the request query pane

## Environment variables
Environment variables can be changed using the gear icon in the app var of a collection. These can be used in the url with {{}}
e.g. {{googleUrl}}/:pathVar
The environment variable in the selected environment could be https://www.google.com
